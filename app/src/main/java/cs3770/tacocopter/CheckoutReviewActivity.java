package cs3770.tacocopter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class CheckoutReviewActivity extends Activity {

    CustomListViewAdapter listAdapter;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout_review);
        init();
    }

    private void init() {
        TextView tv = new TextView(this);
        tv.setText("Cart Contents");

        tv.setTextSize(getResources().getDimension(R.dimen.headerTextSize));
        tv.setGravity(Gravity.CENTER_HORIZONTAL);

        Button finishButton = new Button(this);
        finishButton.setGravity(Gravity.CENTER);
        finishButton.setText("Place Order");
        finishButton.setBackground(ContextCompat.getDrawable(this, R.drawable.rounded_square));
        finishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startCheckOutActivity();
            }
        });
        finishButton.setTextColor(Color.WHITE);
        finishButton.setTextSize(getResources().getDimension(R.dimen.buttonTextSize));

        TextView priceView = new TextView(this);
        priceView.setGravity(Gravity.CENTER);
        priceView.setText(String.format("Total Cost: $%.2f", ShoppingCart.getTotalCost()));
        priceView.setBackground(ContextCompat.getDrawable(this, R.drawable.rounded_square));
        priceView.setTextColor(ContextCompat.getColor(this, R.color.red));
        priceView.setTextSize(getResources().getDimension(R.dimen.totalPriceTextSize));

        listView = (ListView) this.findViewById(R.id.reviewLV);
        listAdapter = new CustomListViewAdapter(this);

        listAdapter.setTotalPriceTextView(priceView);

        listAdapter.addItems(new ArrayList<>(ShoppingCart.getTacoList()));

        listView.setAdapter(listAdapter);
        listView.setHeaderDividersEnabled(false);
        listView.setDividerHeight(20);
        listView.setFooterDividersEnabled(false);
        listView.addHeaderView(tv);
        listView.addFooterView(priceView);
        listView.addFooterView(finishButton);
    }

    public void startCheckOutActivity() {
        if (ShoppingCart.getNumberOfTacos() < 1) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            //builder.setTitle("Your cart is empty!");
            builder.setTitle("You ain't got any tacos, Paco");
            builder.setMessage("It appears you don't have any tacos to be ordered!");
            builder.setNegativeButton("Whoops, let me add some",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                            dialog.cancel();
                        }
                    });

            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            Intent intent = new Intent(getApplicationContext(), CheckoutInfo.class);
            startActivity(intent);
        }
    }

    private void createDialogForNoTacos() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //builder.setTitle("Your cart is empty!");
        builder.setTitle("You ain't got no tacos, Paco");
        builder.setMessage("It appears you don't have any tacos to be ordered!");
        builder.setNegativeButton("Whoops, let me add some",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
