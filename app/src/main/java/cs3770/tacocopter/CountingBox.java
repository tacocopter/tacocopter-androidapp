package cs3770.tacocopter;

import android.content.Context;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

public class CountingBox extends LinearLayout {
    private LayoutInflater mInflator;
    private Button mDecrementButton;
    private Button mIncrementButton;
    private TextView tv;
    private int mCount;
    private TextWatcher tw;

    public CountingBox(Context context) {
        super(context);
        init(context, null, 0);
    }

    public CountingBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public CountingBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr)
    {
        mInflator = LayoutInflater.from(context);
        mInflator.inflate(R.layout.countingbox_layout, this, true);

        mCount = 0;
    }

    @Override
    protected void onFinishInflate()
    {
        super.onFinishInflate();

        mDecrementButton = (Button)this.findViewById(R.id.decrementButton);
        mDecrementButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCount > 0)
                {
                    mCount--;
                    updateCount();
                }
            }
        });

        mIncrementButton = (Button)this.findViewById(R.id.incrementButton);
        mIncrementButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCount < 50) {
                    mCount++;
                    updateCount();
                }
            }
        });


        tv = (TextView) this.findViewById(R.id.valueCapture);
    }

    protected void updateCount()
    {
        TextView currentValue;
        currentValue = (TextView)this.findViewById(R.id.valueCapture);
        currentValue.setText(String.valueOf(mCount));
    }

    public void setCount(int newCount) {
        TextView currentValue;
        currentValue = (TextView)this.findViewById(R.id.valueCapture);
        currentValue.setText(String.valueOf(newCount));
        mCount = newCount;
    }

    public int getCount()
    {
        return mCount;
    }

    public void setTextChangedListener(TextWatcher tw) {
        tv.addTextChangedListener(tw);
        this.tw = tw;
    }

    public void removeTextChangedListener() {
        tv.removeTextChangedListener(tw);
    }

}
