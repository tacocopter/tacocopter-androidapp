package cs3770.tacocopter;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.PopupWindow.OnDismissListener;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class OrderScreen extends AppCompatActivity {

    private final int CHECKOUT_FLAG = 1;

    private final String PREDESIGNED = "Chef Creations";

    ListView expListView;
    OrderScreenListAdapter expListAdapter;

    PopupWindow popup;
    CustomListViewAdapter popupAdapter;
    View popUpView;
    ListView popupListVIew;

    private FrameLayout forground;

    int cartNotificationNumber = 0;
    private TextView notificationTextView = null;

    private Button customTacoButton;
    private Button addToCartButton;
    private Button addToCartAndCheckoutButton;

    private View bottomButtonsView;

    private List<String> groupList;
    private List<String> childList;
    private Map<String, List<Taco>> selections;
    private String[] preDesignedTacoNames = {"The Exploder", "The Patrick", "Just Lettuce", "TacoTaco"};
    private String[][] predesignedTacoIngredients = new String[][] {{"Beef", "Tomatoes", "Cheddar", "Hot Sauce"},
            {"Chicken", "Salmon", "Cheddar", "Lettuce", "Big Mac Sauce"}, {"Lettuce"}, {"Chicken", "Corn", "Cheddar", "Ranch"}};

    private List<Taco> preDesignedTacos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_screen);
        forground = (FrameLayout) this.findViewById(R.id.mainLayout);
        if (forground != null && forground.getForeground() != null) {
            forground.getForeground().setAlpha(0);
        }
        expListView = (ListView) this.findViewById(R.id.expandableListView);
        setupList();
        setupPopupWindow();
    }

    @Override public boolean onCreateOptionsMenu(final Menu menu) {
        MenuInflater inflater = this.getMenuInflater();
        inflater.inflate(R.menu.cart_menu, menu);

        final View menuCart = menu.findItem(R.id.action_settings).getActionView();

        notificationTextView = (TextView) menuCart.findViewById(R.id.hotlist_hot);

        updateCartNotification();

        menuCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forground.getForeground().setAlpha(128);
                popup.showAtLocation(v, Gravity.CENTER, 0, 0);
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void setupList() {

        setupButtons();

        setupPredesignedTacos();

        createGroupList();
        createCollection();


        expListAdapter = new OrderScreenListAdapter(this);

        expListAdapter.addItems(selections.get(PREDESIGNED));

        expListView.setAdapter(expListAdapter);
        expListView.setDividerHeight(20);
        expListView.addFooterView(bottomButtonsView);
        expListView.addHeaderView(customTacoButton);

    }

    private void setupButtons() {
        bottomButtonsView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.custom_taco_button, null);
        customTacoButton = (Button) bottomButtonsView.findViewById(R.id.customButton);

        customTacoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startCustomOrder(v);
            }
        });

        bottomButtonsView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.add_to_cart_and_checkout_button, null);
        addToCartButton = (Button) bottomButtonsView.findViewById(R.id.addToCart);

        addToCartAndCheckoutButton = (Button) bottomButtonsView.findViewById(R.id.checkout);

        addToCartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToCart(v);
            }
        });

        addToCartAndCheckoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkout(v);
            }
        });
    }

    // call the updating code on the main thread,
    // so we can call this asynchronously
    public void updateCartNotification() {
        cartNotificationNumber = ShoppingCart.getNumberOfTacos();
        if (notificationTextView == null) return;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (cartNotificationNumber == 0)
                    notificationTextView.setVisibility(View.INVISIBLE);
                else {
                    notificationTextView.setVisibility(View.VISIBLE);
                    notificationTextView.setText(Integer.toString(cartNotificationNumber));
                }
            }
        });
    }

    public void checkout(View view) {
        //We should check to see if they wanted anymore
        if (ShoppingCart.getNumberOfTacos() < 1 && getTacos().size() < 1) {
        //No tacos in the cart and no tacos on the order
            createDialogForNoTacos();
        }
        else if (getTacos().size() > 0) {
            createDialogForUnaddedTacos(view);
        } else {
            startCheckoutActivity();
        }
    }

    private void startCheckoutActivity() {
        Intent intent = new Intent(getApplicationContext(), CheckoutReviewActivity.class);
        startActivityForResult(intent, CHECKOUT_FLAG);
    }

    private void createDialogForNoTacos() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //builder.setTitle("Your cart is empty!");
        builder.setTitle("You ain't got any tacos, Paco");
        builder.setMessage("It appears you don't have any tacos to be ordered!");
        builder.setNegativeButton("Whoops, let me add some",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void createDialogForUnaddedTacos(final View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Lose out on delicious tacos?");
        builder.setMessage("You have some items you haven't added to your cart");
        builder.setPositiveButton("Don't care go to checkout anyways",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        resetTacoListings();
                        checkout(v);
                    }
                });

        builder.setNeutralButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        builder.setNegativeButton("Add and Checkout",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        addToCart(null);
                        startCheckoutActivity();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void startCustomOrder(View view) {
        Intent intent = new Intent(getApplicationContext(), CustomTacoActivity.class);
        startActivityForResult(intent, 0);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("Order Screen", "onActivityResult called Result code: " + resultCode);

        if (requestCode == CHECKOUT_FLAG) {
            //We added stuff to the cart presumably
            popupAdapter.update();
            updatePopupWindow();
            updateCartNotification();
        } else {
            updatePopupWindow();
            updateCartNotification();
        }
    }


    public List<Taco> getTacos() {
        Map<Integer, CountingBox> map = expListAdapter.getMap();

        List<Taco> tacos = new ArrayList<>();

//        for (int i = 0; i < expListAdapter.getCount(); i++) {
//            if (map.get(i) != null) {
//                Taco taco = expListAdapter.getItem(i);
//                Taco purchased = new Taco(taco);
//                purchased.setNumber(map.get(i).getCount());
//
//                if (purchased.getNumTacos() > 0) {
//                    tacos.add(purchased);
//                }
//                Log.e("OrderScreen", "Taco: " + taco.getName() + " count: " + map.get(i).getCount());
//            }
//        }

        for (Integer key : map.keySet()) {
            if (map.get(key) != null) {
                Taco taco = expListAdapter.getItem(key);
                Taco purchased = new Taco(taco);
                purchased.setNumber(map.get(key).getCount());

                if (purchased.getNumTacos() > 0) {
                    tacos.add(purchased);
                }
                Log.e("OrderScreen", "Taco: " + taco.getName() + " count: " + map.get(key).getCount());
            }
        }

        return tacos;
    }

    public void addToCart(View view) {

        for (Taco taco : getTacos()) {
            ShoppingCart.addItem(taco);
        }

        resetTacoListings();
        updatePopupWindow();
        updateCartNotification();
    }

    private void setupPredesignedTacos() {
        preDesignedTacos = new ArrayList<>(3);
        for (int i = 0; i < preDesignedTacoNames.length; i++) {
            ArrayList<String> toppings = new ArrayList<>(Arrays.asList(predesignedTacoIngredients[i]));
            Taco taco = new Taco(toppings, toppings.size(), preDesignedTacoNames[i]);
            preDesignedTacos.add(taco);
        }

    }

    private void resetTacoListings() {
        Map<Integer, CountingBox> map = expListAdapter.getMap();

        List<Taco> tacos = new ArrayList<>();

//        for (int i = 0; i < expListAdapter.getCount(); i++) {
//            map.get(i).setCount(0);
////            Log.e("OrderScreen", "Taco: " + taco.getName() + " count: " + map.get(i).getCount());
//        }

        for (Integer key : map.keySet()) {
            map.get(key).setCount(0);
        }
    }

    private void createGroupList() {
        groupList = new ArrayList<>();
        groupList.add(PREDESIGNED);
    }

    private void createCollection() {
        selections = new LinkedHashMap<>();

        //We only have one header
        selections.put(PREDESIGNED, preDesignedTacos);
    }

    private void setupPopupWindow() {
        Drawable colorDrawableBackground = new ColorDrawable(ContextCompat.getColor(this, R.color.Background));

        popUpView = getLayoutInflater().inflate(R.layout.popup_view, null, false);

        popupListVIew = (ListView) popUpView.findViewById(R.id.lv);
        popupAdapter = new CustomListViewAdapter(this);

        updatePopupWindow();

        popupListVIew.setDivider(colorDrawableBackground);
        popupListVIew.setAdapter(popupAdapter);
        popupListVIew.setHeaderDividersEnabled(false);
        popupListVIew.setDividerHeight(20);

        Button checkoutFromCartButton = new Button(this);
        checkoutFromCartButton.setGravity(Gravity.CENTER);
        checkoutFromCartButton.setText("Checkout");
        checkoutFromCartButton.setBackground(ContextCompat.getDrawable(this, R.drawable.rounded_square));
        checkoutFromCartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkout(v);
            }
        });
        checkoutFromCartButton.setTextColor(Color.WHITE);
        checkoutFromCartButton.setTextSize(getResources().getDimension(R.dimen.buttonTextSize));

        TextView priceView = new TextView(this);
        priceView.setGravity(Gravity.CENTER);
        priceView.setText(String.format("Total Cost: $%.2f", ShoppingCart.getTotalCost()));
        priceView.setBackground(ContextCompat.getDrawable(this, R.drawable.rounded_square));
        priceView.setTextColor(ContextCompat.getColor(this, R.color.red));
        priceView.setTextSize(getResources().getDimension(R.dimen.totalPriceTextSize));

        popupAdapter.setTotalPriceTextView(priceView);

        popup = new PopupWindow(popUpView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        popup.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss() {
                forground.getForeground().setAlpha(0);
                updateCartNotification();
            }
        });


        popup.setFocusable(true);
        popupListVIew.addFooterView(priceView);
        popupListVIew.addFooterView(checkoutFromCartButton);

    }

    public void updatePopupWindow() {
        popupAdapter.addItems(new ArrayList<>(ShoppingCart.getTacoList()));
    }

    private void loadChild(String[] selections) {
        childList = new ArrayList<>(Arrays.asList(selections));
    }
}