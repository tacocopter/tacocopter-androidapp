package cs3770.tacocopter;

import android.content.Context;
import android.graphics.Color;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class DescriptionAndCounterBox extends RelativeLayout {

    private LayoutInflater inflater;
    private TextView title;
    private TextView description;
    private CharSequence titleText = "TACO";
    private CharSequence descriptionText = "A mother fucking taco" ;
    private CountingBox countingBox;

    public DescriptionAndCounterBox(Context context) {
        super(context);
        init(context);
    }

    public DescriptionAndCounterBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public DescriptionAndCounterBox(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public void init(Context context) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.description_and_counter_box, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        title = (TextView) this.findViewById(R.id.description_title);
        description = (TextView) this.findViewById(R.id.description_content);
        title.setText(titleText);
        description.setText(descriptionText);
        countingBox = (CountingBox) this.findViewById(R.id.view);

    }

    public void setTitle(CharSequence newTitle) {
        titleText = newTitle;
        title.setText(titleText);
    }

    public void setDescription(CharSequence newDescription) {
        descriptionText = newDescription;
        description.setText(descriptionText);
    }

    public int getCount() {
        return countingBox.getCount();
    }

    public void setCounterChangedListener(TextWatcher tw) {
        countingBox.setTextChangedListener(tw);
    }

    public void removeCounterChangedListener() {
        countingBox.removeTextChangedListener();
    }

}
