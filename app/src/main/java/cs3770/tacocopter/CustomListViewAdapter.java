package cs3770.tacocopter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class CustomListViewAdapter extends BaseAdapter {

    protected static class ViewHolder {
        private TextView title;
        private TextView description;
        private TextView price;
        private ImageButton button;
    }

    private ArrayList<Taco> mData = new ArrayList<>();
    private LayoutInflater mInflater;
    private Activity context;

    private TextView totalPrice;

    public CustomListViewAdapter(Activity context) {
        this.context = context;
        mInflater = context.getLayoutInflater();
    }

    public void setTotalPriceTextView(TextView totalPrice) {
        this.totalPrice = totalPrice;
    }

    public void addItem(final Taco item) {
        mData.add(item);
        notifyDataSetChanged();
        updatePrice();
    }

    public void addItems(final List<Taco> tacos) {
        mData = new ArrayList<>(tacos);
        notifyDataSetChanged();
        updatePrice();
    }

    public void removeItem(int position) {
        updateCart(position);
        mData.remove(position);
        notifyDataSetChanged();
        updatePrice();
    }

    public void update() {
        notifyDataSetChanged();
        updatePrice();
    }

    private void updateCart(int position) {
        ShoppingCart.removeItem(mData.get(position));
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Taco getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {

            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.taco_item_with_delete_view, null);

            holder.title = (TextView) convertView.findViewById(R.id.title);

            holder.description = (TextView) convertView.findViewById(R.id.description);

            holder.price = (TextView) convertView.findViewById(R.id.price);

            holder.button = (ImageButton) convertView.findViewById(R.id.button);

            Log.e("getView", "Setting " + mData.get(position).getName() + " to position: " + String.valueOf(position));

            holder.button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Taco taco = getItem(position);
                    taco.setNumber(taco.getNumTacos() - 1);

                    Log.e("Removing items", String.valueOf(position) + " " + taco.getName());

                    if (taco.getNumTacos() < 1) {
                        removeItem(position);
                    } else {
                        notifyDataSetChanged();
                        updatePrice();
                    }
                }
            });

            convertView.setTag(holder);
        }


        else {
            holder = (ViewHolder) convertView.getTag();

            holder.button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Taco taco = getItem(position);
                    taco.setNumber(taco.getNumTacos() - 1);

                    Log.e("Removing items", String.valueOf(position) + " " + taco.getName());

                    if (taco.getNumTacos() < 1) {
                        removeItem(position);
                    } else {
                        notifyDataSetChanged();
                        updatePrice();
                    }
                }
            });

            Log.e("getView", "Setting " + mData.get(position).getName() + " to position: " + String.valueOf(position));
        }

        if (mData.get(position).getNumTacos() < 1) {
            notifyDataSetChanged();
        }

        holder.title.setText(mData.get(position).getTitleWithAmount());
        holder.description.setText(mData.get(position).getToppingsAsReadableString());

        Log.e("getView", "Position " + String.valueOf(position) + " belongs to: " + mData.get(position).getName());

        String output = String.format("Individual Cost: $%.2f\nTotal Cost: $%.2f", mData.get(position).getIndividualCost(), mData.get(position).getTotalCost());

        holder.price.setText(output);

        return convertView;
    }

    private void updatePrice() {
        if (totalPrice != null) {
            totalPrice.setText(String.format("Total Cost: $%.2f", ShoppingCart.getTotalCost()));
        }
    }

}