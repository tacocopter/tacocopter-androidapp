package cs3770.tacocopter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomTacoExpandableListAdapter extends BaseExpandableListAdapter {
    private Activity context;
    private Map<String, List<String>> selections;
    private List<String> options;

    private HashMap<Integer, boolean[]> mChildCheckStates;

    private TextView individualPrice;
    private TextView totalPrice;

    private double price;
    private int numTacos;

    public CustomTacoExpandableListAdapter(Activity context, List<String> options,
                                     Map<String, List<String>> selections) {
        this.context = context;
        this.selections = selections;
        this.options = options;

        mChildCheckStates = new HashMap<>();
        price = 0.0;

    }

    public void setNumTacos(int numTacos) {
        this.numTacos = numTacos;
    }

    public Object getChild(int groupPosition, int childPosition) {
        return selections.get(options.get(groupPosition)).get(childPosition);
    }

    public void setTextViews(TextView total, TextView individual) {
        individualPrice = individual;
        totalPrice = total;
    }

    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }


    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        final String option = (String) getChild(groupPosition, childPosition);
        LayoutInflater inflater = context.getLayoutInflater();

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.custom_child_item, null);
        }

        CheckBox item = (CheckBox) convertView.findViewById(R.id.customCheckBox);

        item.setOnCheckedChangeListener(null);

        if (mChildCheckStates.containsKey(groupPosition)) {
            //mChildCheckStates is a hashmap with a groupPosition key and a boolean array value
            boolean getChecked[] = mChildCheckStates.get(groupPosition);

            // set the check state of this position's checkbox based on the
            // boolean value of getChecked[position]
            item.setChecked(getChecked[childPosition]);

        } else {

            // We haven't made a boolean array for this group yet, so lets do it
            boolean getChecked[] = new boolean[getChildrenCount(groupPosition)];

            // add getChecked[] to the mChildCheckStates hashmap using mGroupPosition as the key
            mChildCheckStates.put(groupPosition, getChecked);

            // set the check state of this position's checkbox based on the
            // boolean value of getChecked[position]
            item.setChecked(false);
        }

        item.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if (isChecked) {

                    boolean getChecked[] = mChildCheckStates.get(groupPosition);
                    getChecked[childPosition] = true;
                    mChildCheckStates.put(groupPosition, getChecked);

                } else {

                    boolean getChecked[] = mChildCheckStates.get(groupPosition);
                    getChecked[childPosition] = false;
                    mChildCheckStates.put(groupPosition, getChecked);
                }
                updateIndividualPrice();
            }
        });


        String itemText = option + String.format("  ($%.2f)", Taco.prices.get(option));

        item.setTextColor(ContextCompat.getColor(context, R.color.DarkBlueText));
        item.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);

        item.setText(itemText);
        return convertView;
    }

    public int getChildrenCount(int groupPosition) {
        return selections.get(options.get(groupPosition)).size();
    }

    public Object getGroup(int groupPosition) {
        return options.get(groupPosition);
    }

    public int getGroupCount() {
        return options.size();
    }

    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String selection = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.custom_group_item,
                    null);
        }
        TextView item = (TextView) convertView.findViewById(R.id.type);

        item.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 40);
        item.setTextColor(ContextCompat.getColor(context, R.color.DarkBlueText));
        item.setText(selection);
        item.setGravity(Gravity.CENTER);
        return convertView;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public HashMap<Integer, boolean[]> getSelected() {
        return new HashMap<>(mChildCheckStates);
    }

    public void updateIndividualPrice() {
        price = 0.0;
        HashMap<Integer, boolean[]> selected = getSelected();
        for (int group = 0; group < getGroupCount(); group++) {
            for (int child = 0; child < getChildrenCount(group); child++) {
                String topping = (String) getChild(group, child);
                if (selected.containsKey(group) && child < selected.get(group).length && selected.get(group)[child]) {
                    price += Taco.prices.get(topping);
                }
            }
        }

        individualPrice.setText(String.format("Taco Cost: $%.2f", price));
        updateTotalPrice();
    }

    public void updateTotalPrice() {
        totalPrice.setText(String.format("Total Cost: $%.2f", price * numTacos));
    }
}
