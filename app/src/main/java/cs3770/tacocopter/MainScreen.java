package cs3770.tacocopter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.content.Intent;

public class MainScreen extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main_screen);
    }

    /* called when the user clicks the order tacos button */
    public void orderTacos(View view)
    {
        Intent intent = new Intent(this, OrderScreen.class);
        startActivity(intent);
    }
}
