package cs3770.tacocopter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class CheckoutInfo extends AppCompatActivity {
    private LatLng mdestinationLocation = new LatLng(0, 0);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout_info);

        //set the contents of the credit card types
        Spinner cardType = (Spinner) findViewById(R.id.cardTypeSelector);
        ArrayAdapter<CharSequence> cardTypeAdapter = ArrayAdapter.createFromResource(this,
                R.array.creditcards_array, android.R.layout.simple_spinner_item);
        cardTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cardType.setAdapter(cardTypeAdapter);

        //set the contents of the expiration month
        Spinner expMonth = (Spinner) findViewById(R.id.expMonth);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.months_array,
                android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        expMonth.setAdapter(adapter);

        //set the contents of the expiration day
        Spinner expYear = (Spinner) findViewById(R.id.expYear);
        ArrayList<String> years = new ArrayList<>();
        for (int i = 2016; i <= 2030; ++i) {
            years.add(Integer.toString(i));
        }
        ArrayAdapter<String> dayAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, years);
        dayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        expYear.setAdapter(dayAdapter);

        Button contButton = (Button) findViewById(R.id.contButton);
        contButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                continueButtonClick(v);
            }
        });
    }

    protected void asyncGetLocationData() {
        //run the URL connection processes on a separate thread
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                getLoacationData();
            }
        });

        thread.start();
        try {
            //wait for this thread to finish before continuing
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected void getLoacationData() {
        EditText streetAddressEdit = (EditText) findViewById(R.id.streetAddress);
        String streetAddress = streetAddressEdit.getText().toString();

        EditText cityEdit = (EditText) findViewById(R.id.city);
        String city = cityEdit.getText().toString();

        EditText postalCodeEdit = (EditText) findViewById(R.id.postalCode);
        String postalCode = postalCodeEdit.getText().toString();

        String fullAddress = streetAddress + ", " + city + ", " + postalCode;

        //LatLng locationPoint = getLocationFromAddress(getApplicationContext(), fullAddress);
        JSONObject obj = getLocationInfo(fullAddress);
        setLatLong(obj);
    }

    protected void continueButtonClick(View view) {
        asyncGetLocationData();

        if (mdestinationLocation.latitude != 0 || mdestinationLocation.longitude != 0) {
            Intent intent = new Intent(this, DeliveryEstimation.class);
            Bundle extras = new Bundle();
            extras.putParcelable("destination_point", mdestinationLocation);
            intent.putExtra("bundle", extras);
            startActivity(intent);
        } else {
            createDialogForBadAddress();
        }
    }

    private void createDialogForBadAddress() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Oops, please enter a valid address!");
        builder.setMessage("The address you have entered appears to have a mistake");
        builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    // Method that uses Geocoder API which is inconsistent
/*    protected LatLng getLocationFromAddress(Context context, String strAddress) {
        Geocoder coder = new Geocoder(context);
        List<Address> addresses;
        LatLng location = new LatLng(0, 0);

        try {
            addresses = coder.getFromLocationName(strAddress, 3);
            if (addresses == null) {
                return null;
            }

            Address strLocation = addresses.get(0);
            location = new LatLng(strLocation.getLatitude(), strLocation.getLongitude());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }*/

    //create a JSONObject that contains the text from querying the maps.google.com api directly with address
    protected JSONObject getLocationInfo(String address) {
        StringBuilder stringBuilder = new StringBuilder();
        address = address.replaceAll(" ", "%20");

        URL url;
        HttpURLConnection urlConnection = null;
        try {
            url = new URL("http://maps.google.com/maps/api/geocode/json?address=" + address + "&sensor=false");

            urlConnection = (HttpURLConnection) url.openConnection();

            InputStream in = urlConnection.getInputStream();

            InputStreamReader isw = new InputStreamReader(in);

            int data = isw.read();
            while (data != -1) {
                char current = (char) data;
                stringBuilder.append(current);
                data = isw.read();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject(stringBuilder.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    //parse the jsonObject for longitude and latitude
    protected void setLatLong(JSONObject jsonObject) {

        Double lon = new Double(0);
        Double lat = new Double(0);

        if (jsonObject != null) {
            try {

                lon = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONObject("geometry").getJSONObject("location")
                        .getDouble("lng");

                lat = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONObject("geometry").getJSONObject("location")
                        .getDouble("lat");

            } catch (Exception e) {
                e.printStackTrace();

            }
        }

        //set our member location
        mdestinationLocation = new LatLng(lat, lon);
    }
}
