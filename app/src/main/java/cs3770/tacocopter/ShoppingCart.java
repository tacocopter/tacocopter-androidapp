package cs3770.tacocopter;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {
    private static ShoppingCart ourInstance = new ShoppingCart();

    public static ShoppingCart getInstance() {
        return ourInstance;
    }

    private static List<Taco> cartContents = new ArrayList<>();

    public static List<Taco> getTacoList() { return cartContents; }

    public static void addItem(Taco taco) {
        //Taco doesn't compare taco number. Just toppings and name
        if (cartContents.contains(taco)) {
            //We some of this taco type already. So let's combine them
            Taco oldTaco = cartContents.get(cartContents.indexOf(taco));
            oldTaco.setNumber(oldTaco.getNumTacos() + taco.getNumTacos());
        } else {
            //We don't have a taco of this type
            cartContents.add(taco);
        }
    }

    public static void removeItem(Taco taco) {
        cartContents.remove(taco);
    }

    public static int getSize() {
        return cartContents.size();
    }

    public static int getNumberOfTacos() {
        int numTacos = 0;
        for (Taco taco : cartContents) {
            numTacos += taco.getNumTacos();
        }

        return numTacos;
    }

    public static double getTotalCost() {
        double totalCost = 0.0;
        for (Taco taco : cartContents) {
            totalCost += taco.getTotalCost();
        }

        return totalCost;
    }

    public static void clearCart() {
        cartContents.clear();
    }
    private ShoppingCart() {
    }
}
