package cs3770.tacocopter;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public class DeliveryEstimation extends AppCompatActivity {
    private final LatLng HOME_BASE = new LatLng(49.6878, -112.8161);
    private final int BASE_PREP_TIME = 4;
    private final int TIME_PER_TACO = 1;
    private final double SPEED = 16.0; //m per sec

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_estimation);

        LatLng deliveryLocation = getLocation();
        float[] results = new float[1];
        Location.distanceBetween(HOME_BASE.latitude, HOME_BASE.longitude,
                deliveryLocation.latitude, deliveryLocation.longitude, results);

        int deliveryTime = getDeliveryTime(results[0]);
        deliveryTime += BASE_PREP_TIME;
        deliveryTime += (TIME_PER_TACO * ShoppingCart.getNumberOfTacos());
        TextView timeDisplay = (TextView) findViewById(R.id.deliveryTime);
        String dT = Integer.toString(deliveryTime) + "min";
        timeDisplay.setText(dT);

        Button orderAgainButton = (Button) findViewById(R.id.orderMoreTacosButton);
        orderAgainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderMoreTacosButton_click();
            }
        });
    }

    protected LatLng getLocation() {
        LatLng location = new LatLng(0, 0);
        Bundle info = getIntent().getParcelableExtra("bundle");
        if (info != null) {
            location = info.getParcelable("destination_point");
        }

        return location;
    }

    protected int getDeliveryTime(float distance) {
        int time = (int) Math.floor(((distance / SPEED) + 0.5));
        time /= 60;

        return time; //minutes
    }

    protected void orderMoreTacosButton_click() {
        ShoppingCart.clearCart();

        Intent intent = new Intent(this, MainScreen.class);
        startActivity(intent);
    }
}
