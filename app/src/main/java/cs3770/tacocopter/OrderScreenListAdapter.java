package cs3770.tacocopter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;


public class OrderScreenListAdapter extends BaseAdapter {


    protected static class ViewHolder {
        public TextView title;
        public TextView description;
        public TextView price;
        public CountingBox cb;
    }

    private ArrayList<Taco> mData = new ArrayList<>();
    private LayoutInflater mInflater;
    private Activity context;

    private Map<Integer, CountingBox> map;

    public OrderScreenListAdapter(Activity context) {
        this.context = context;
        mInflater = context.getLayoutInflater();
        map = new HashMap<>();
    }

    public void addItem(final Taco item) {
        mData.add(item);
        notifyDataSetChanged();
    }

    public void addItems(final List<Taco> tacos) {
        mData = new ArrayList<>(tacos);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Taco getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {

            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.description_and_counter_box, null);

            holder.title = (TextView) convertView.findViewById(R.id.description_title);

            holder.description = (TextView) convertView.findViewById(R.id.description_content);

            holder.price = (TextView) convertView.findViewById(R.id.price_box);

            holder.cb = (CountingBox) convertView.findViewById(R.id.view);


            convertView.setTag(holder);
        }

        else {
            holder = (ViewHolder) convertView.getTag();
        }


        map.put(position, holder.cb);

        holder.title.setText(mData.get(position).getName());
        holder.description.setText(mData.get(position).getToppingsAsReadableString());
        holder.price.setText(String.format("Cost: $%.2f", mData.get(position).getIndividualCost()));

        return convertView;
    }

    public Map<Integer, CountingBox> getMap() {
        return map;
    }

}
