package cs3770.tacocopter;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class CustomTacoActivity extends AppCompatActivity {

    final String NAME = "CUSTOM";

    ExpandableListView expListView;
    CustomTacoExpandableListAdapter expListAdapter;

    TextView totalTacoPriceTV;
    TextView individualTacoPriceTV;

    private static String[] toppingTypes = {"Meat", "Vegetables", "Cheese", "Sauces"};
    String[] meatSelections  = {"Beef", "Chicken", "Salmon", "Lettuce"};
    String[] vegetablesSelections = {"Tomatoes", "Lettuce", "Corn"};
    String[] cheeseSelections = {"Cheddar", "Havarti", "Brie"};
    String[] sauceSelections = {"Ranch", "Salsa", "Big Mac Sauce"};

    private List<String> toppingTypeList;
    private List<String> toppingList;
    private Map<String, List<String>> selections;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_taco);

        expListView = (ExpandableListView) this.findViewById(R.id.expandableListView);
        createGroupList();
        createCollection();

        CountingBox cb = (CountingBox) this.findViewById(R.id.cb);

        if (cb != null) {
            cb.setCount(1);
        }

        totalTacoPriceTV = (TextView) this.findViewById(R.id.totalTacoPrice);
        individualTacoPriceTV = new TextView(this);
        individualTacoPriceTV.setText(String.format("Taco Cost: $%.2f", 0.0));
        individualTacoPriceTV.setTextColor(ContextCompat.getColor(this, R.color.red));
        individualTacoPriceTV.setTextSize(getResources().getDimension(R.dimen.individualPriceTextSize));
        individualTacoPriceTV.setPadding(getResources().getDimensionPixelSize(R.dimen.innerPadding), 0, 0, 0);

        totalTacoPriceTV.setGravity(Gravity.CENTER);
        totalTacoPriceTV.setText(String.format("Total Cost: $%.2f", ShoppingCart.getTotalCost()));
        totalTacoPriceTV.setTextColor(ContextCompat.getColor(this, R.color.red));
        totalTacoPriceTV.setTextSize(getResources().getDimension(R.dimen.totalPriceTextSize));

        expListAdapter = new CustomTacoExpandableListAdapter(
                this, toppingTypeList, selections);

        expListAdapter.setNumTacos(1);


        cb.setTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (expListAdapter != null) {
                    expListAdapter.setNumTacos(Integer.valueOf(s.toString()));
                    expListAdapter.updateTotalPrice();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        expListAdapter.setTextViews(totalTacoPriceTV, individualTacoPriceTV);

        expListView.setAdapter(expListAdapter);
        expListView.addFooterView(individualTacoPriceTV);
    }

    private void createGroupList() {
        toppingTypeList = new ArrayList<>(Arrays.asList(toppingTypes));
    }

    public void addToCart(View view) {
        //Prepare the taco to be added to the cart
        List<String> toppings = new ArrayList<>();
        Map<Integer, boolean[]> selected = new HashMap<>(expListAdapter.getSelected());
        for (int toppingType = 0; toppingType < toppingTypes.length; toppingType++) {
            if (selected.containsKey(toppingType)) {
                boolean[] toppingsSelected = selected.get(toppingType);

                switch (toppingType) {
                    case (0):
                        addToppings(toppings, toppingsSelected, meatSelections);
                        break;
                    case (1):
                        addToppings(toppings, toppingsSelected, vegetablesSelections);
                        break;
                    case (2):
                        addToppings(toppings, toppingsSelected, cheeseSelections);
                        break;
                    case (3):
                        addToppings(toppings, toppingsSelected, sauceSelections);
                        break;
                    default:
                        break;
                }
            }
        }

        int count = 0;
        CountingBox cb = ((CountingBox) this.findViewById(R.id.cb));
        if (cb != null) {
            count = cb.getCount();
        }

        Taco newTaco = new Taco(toppings, count, NAME);

        ShoppingCart.addItem(newTaco);

        setResult(RESULT_OK);
        finish();
    }

    private void addToppings(List<String> list, boolean[] selected, String[] toppings) {
        if (toppings.length != selected.length) {
            return;
        }

        for (int i = 0; i < toppings.length; i++) {
            if (selected[i]) {
                list.add(toppings[i]);
            }
        }
    }

    private void createCollection() {
        selections = new LinkedHashMap<>();

        for (String option : toppingTypes) {
            switch (option) {
                case("Meat"):
                    loadChild(meatSelections);
                    break;
                case("Vegetables"):
                    loadChild(vegetablesSelections);
                    break;
                case("Cheese"):
                    loadChild(cheeseSelections);
                    break;
                case("Sauces"):
                    loadChild(sauceSelections);
                    break;
                default:
                    break;
            }

            selections.put(option, toppingList);
        }
    }

    private void loadChild(String[] selections) {
        toppingList = new ArrayList<>(Arrays.asList(selections));
    }
}
