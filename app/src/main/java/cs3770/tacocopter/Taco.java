package cs3770.tacocopter;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Taco {

//    String[] meatSelections  = {"Beef", "Chicken", "Salmon", "Lettuce"};
//    String[] vegetablesSelections = {"Tomatoes", "Lettuce", "Corn"};
//    String[] cheeseSelections = {"Cheddar", "Havarti", "Brie"};
//    String[] sauceSelections = {"Ranch", "Salsa", "Big Mac Sauce"};

    public static Map<String, Double> prices;
    static {
        prices = new HashMap<>();
        prices.put("Beef", 1.0);
        prices.put("Chicken", 1.0);
        prices.put("Salmon", 1.25);
        prices.put("Lettuce", 0.25);
        prices.put("Tomatoes", 0.25);
        prices.put("Corn", 0.25);
        prices.put("Cheddar", 0.5);
        prices.put("Havarti", 1.0);
        prices.put("Brie", 1.0);
        prices.put("Ranch", 0.25);
        prices.put("Salsa", 0.25);
        prices.put("Big Mac Sauce", 0.50);
        prices.put("Hot Sauce", 0.25);
    }

    private String name;
    private List<String> mToppings;
    private int mNumTacos;
    private double individualCost;
    private double totalCost;

    public Taco(List<String> mToppings, int mNumTacos, String name) {
        this.name = name;
        this.mToppings = new ArrayList<>(mToppings);
        this.mNumTacos = mNumTacos;

        individualCost = 0.0;
        for (String topping : mToppings) {
            individualCost += updatePrice(topping);
        }

        totalCost = individualCost * mNumTacos;

    }

    public Taco(Taco taco) {
        this(new ArrayList<>(taco.getToppings()), taco.getNumTacos(), taco.getName());
    }

    public Taco()
    {
        this(new ArrayList<String>(), 0, "Custom");
    }



    public int getNumTacos() {
        return mNumTacos;
    }

    public ArrayList<String> getToppings() {
        return new ArrayList<>(mToppings);
    }

    public void addTopping(String topping)
    {
        if (!mToppings.contains(topping))
        {
            if (prices.containsKey(topping)) {
                mToppings.add(topping);
                individualCost += updatePrice(topping);
                totalCost = individualCost * individualCost;
            } else {
                Log.e("Taco", "Key not found: " + topping);
            }
        }
    }

    private double updatePrice(String topping) {
        double cost = 0.0;
        if (prices.containsKey(topping)) {
            cost += prices.get(topping);
        } else {
            Log.e("Taco", "Key not found: " + topping);
        }
        return cost;
    }

    public void setNumber(int num)
    {
        mNumTacos = num;
        totalCost = individualCost * mNumTacos;
    }

    public void setToppings(ArrayList<String> toppings)
    {
        mToppings = toppings;
        individualCost = 0.0;
        for (String topping : mToppings) {
            individualCost += updatePrice(topping);
        }

        totalCost = individualCost * mNumTacos;
    }

    public String getToppingsAsReadableString() {
        StringBuilder sb = new StringBuilder(200);
        for (String topping : mToppings) {
            sb.append(topping);
            sb.append(System.getProperty("line.separator"));
        }

        sb.deleteCharAt(sb.length()-1);

        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Taco taco = (Taco) o;

        ArrayList<String> toppings1 = getToppings();
        Collections.sort(toppings1);

        ArrayList<String> toppings2 = taco.getToppings();
        Collections.sort(toppings2);

        return toppings1.equals(toppings2) && taco.getName().equals(this.getName());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 31;
        hash +=  mToppings != null ? mToppings.hashCode() : 0;
        hash += name.hashCode();
        hash += mNumTacos;
        return hash;
    }

    public String getTitleWithAmount() {
        StringBuilder sb = new StringBuilder(50);
        sb.append(name);
        sb.append(" x");
        sb.append(String.valueOf(mNumTacos));

        return sb.toString();
    }

    public double getIndividualCost() {
        return individualCost;
    }

    public double getTotalCost() {
        return totalCost;
    }

}
